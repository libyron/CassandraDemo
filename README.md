# CassandraDemo

从 [Cassandra 官方仓库](https://github.com/apache/cassandra)提取出来的 word count 样例程序（release 3.0.16）。
改用 maven 工程格式，明确依赖，使其可以编译运行。

## 编译运行

运行本样例之前，需要在 cassandra 安装目录下运行 `bin/cassandra` , 在 hadoop 安装目录下运行 `sbin/start-all.sh` 启动相关服务。

首先编译并运行 CfAppSetup, 会在 cassandra 中创建 keyspace = cql3_wordcount, column_family = inputs 的表格，并随机插入英文句子。

然后运行 `CfApp` 进行单词统计并输出到 `/tmp/word_count/part-0000`.

## 笔记

1. 通过实际运行发现，key-value 格式不匹配的 InputFormt 和 Mapper 会导致 map 方法不被调用；
2. cassandra 会对 partition key 进行 hash 后确定范围，遍历每个范围进行获取 input split, 即便表很小也要完全便利。

## TODO

### 开发组

实现定制的各类：

#### `CfCqlInputFormat`

InputFormat 外层实现，内部将 `COLUMN_FAMILY` 切分成单独的 cfName.
选择一个 column family 作为 `InputSplit` 基准。

内部实例化 `CfCqlRecordReader`.

#### `CfCqlRecordReader`

内部实例化为每个 column family 实例化一个 `CqlRecordReader`， 使用统一的 Input Split.
将每个 `CqlRecordReader` 的 Row 整合后返回。

#### `CfRow`

`Map<String, Map<String, ByteBuffer>> = [cfName, [columnName, value]]`.

### 测试组

#### 准备表格和输入数据

修改 `CfAppSetup`, 构建符合实验项目要求的 keyspace,
即有多个 column family, 每个 column family 下面一个 column.
并准备测试输入。可以参考 hw2 的 word_count, 也可以构造新的场景。

#### 构建应用场景

修改 `CfApp` 的 `COLUMN_FAMILY`, 用 ':' 拼接多个需要的 column family.
修改 mapper 和 reducer 以实现希望的计算任务。

## 备注

为了关闭 logger warning 并让其正常工作, 在 java 后添加 JVM 参数：

```shell
java -Dlog4j.configuration=file:/path/to/log4j.properties
```
